#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

from setuptools import find_packages, setup

readme = open('README.md').read()
history = open('CHANGES.md').read()
install_requires = [
    'import-string>=0.1.0,<0.2.0',
    'psycopg2>=2.7.5,<2.8.0',
    'pyOpenSSL>=18.0.0,<18.1.0',
    'requests>=2.19.1,<2.20.0',
    'SQLAlchemy>=1.2.10,<1.3.0',
    'stomp.py>=4.1.21,<4.2.0',
]


packages = find_packages()

# Get the version string. Cannot be done with import!
g = {}
with open(os.path.join("cern_web_indexer", "version.py"), "rt") as fp:
    exec(fp.read(), g)
    version = g["__version__"]

setup(
    name='cern-web-indexer',
    version=version,
    description='CERN Web Indexer',
    long_description=readme + '\n\n' + history,
    license='GPLv3',
    author='CERN',
    author_email='cernsearch.support@cern.ch',
    url='http://search.cern.ch/',
    packages=packages,
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=install_requires,
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Development Status :: 1 - Pre-Alpha',
    ],
)
