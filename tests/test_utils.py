#!/usr/bin/python
import pytest

from cern_web_indexer.utils import BucketList


def test_bucketlist():
    bucketlist = BucketList()
    # Insert first element and assert existence.
    bucketlist.put('key1', 'val1')
    assert bucketlist.exists('key1')
    # Insert second element.
    bucketlist.put('key2', 'val2')
    # Remove first element and assert removal.
    bucketlist.remove('key1')
    assert not bucketlist.exists('key1')
    # Create a second bucket list. Test singleton implementation.
    bucketlist_two = BucketList()
    # Assert existance of the second key, inserted in first bucket list.
    assert bucketlist_two.exists('key2')
    # Remove second key and assert removal of both (same) bucket list.
    bucketlist_two.remove('key2')
    assert not bucketlist_two.exists('key2')
    assert not bucketlist.exists('key2')