#!/usr/bin/python
import time

from cern_web_indexer import config
from cern_web_indexer.listeners import FeedListener, DoneListener
from cern_web_indexer.messaging import BrokerConnection

# Create connection
done_broker = BrokerConnection()
feed_broker = BrokerConnection()
# Set Done listener
done_broker.set_listener('done', DoneListener())
done_broker.start()
done_broker.connect()
done_broker.subscribe(config.done_queue, 'done', 'auto')

# Set Feed listener
feed_broker.set_listener('feed', FeedListener())
feed_broker.start()
feed_broker.connect()
feed_broker.subscribe(config.feed_queue, 'feed', 'auto')

# Do not exit program
# Disconnect upon signal
try:
    print("Connected & Listening sir!")
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    print("Disconnecting receiver...")
    time.sleep(1)
    feed_broker.unsubscribe('feed')
    done_broker.unsubscribe('done')
    feed_broker.disconnect()
    done_broker.disconnect()
