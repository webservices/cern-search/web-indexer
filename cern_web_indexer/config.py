#!/usr/bin/python
import os
from cern_web_indexer import utils

########################################
############## MESSAGING ###############
########################################
# Queue
feed_queue = os.getenv('INDEXER_FEED_QUEUE_NAME', '/queue/crawler.feed')
todo_queue = os.getenv('INDEXER_TODO_QUEUE_NAME', '/queue/crawler.todo')
done_queue = os.getenv('INDEXER_DONE_QUEUE_NAME', '/queue/crawler.done')
# Broker
broker_hostname = os.getenv('INDEXER_BROKER_HOSTNAME', 'localhost')
broker_port = os.getenv('INDEXER_BROKER_PORT', 61613)
broker_ssl_cert = os.getenv('INDEXER_SSL_CERTIFICATE_FILE')
broker_loader_function = os.getenv('INDEXER_BROKER_LOADER_FUNCTION', 'cern_web_indexer.messaging:get_broker')
broker_list_loader_function = os.getenv('INDEXER_BROKER_LIST_LOADER_FUNCTION', 'cern_web_indexer.messaging:get_broker_list')

########################################
############# CERN SEARCH ##############
########################################
cernsearch = utils.evaluate_config_param(os.getenv('INDEXER_CERN_SEARCH'))
if not cernsearch:
    cernsearch = {
        'cernsearch-test': {
            'api': 'https://test-cern-search.web.cern.ch/',
            'index': 'cernsearch-test_doc_v0.0.1.json',
            'token': '<TOKEN>'
        }
    }

########################################
######## SQLAlchemy / Database #########
########################################
database_url = utils.evaluate_config_param(os.getenv('INDEXER_DATABASE'))
if not database_url:
    database_url = {
        'drivername': 'postgres',
        'host': 'localhost',
        'port': '5432',
        'username': 'wcrawler',
        'password': 'passwd',
        'database': 'wcrawler'
    }
