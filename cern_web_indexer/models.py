from sqlalchemy import create_engine, Column, String, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL

from cern_web_indexer import config

DeclarativeBase = declarative_base()


def db_connect():
    """
    Performs database connection using database settings from config.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(URL(**config.database_url))


def create_indexer_instance_item_table(engine):
    """"""
    DeclarativeBase.metadata.create_all(engine)


class IndexerWebServicesItem(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = 'indexer_webservices'

    key = Column('key', String, primary_key=True)
    url = Column('url', String, nullable=False)
    name = Column('name', String, nullable=True)
    content = Column('content', String, nullable=True)
    origin = Column('origin', String, primary_key=True)
    last_updated = Column('last_updated', DateTime, nullable=True)
    attfl = Column('attfl', Boolean, nullable=False)  # Attachment / Followed Link


def create_item(item):

    if item['instance'] == 'webservices-search':
        return IndexerWebServicesItem(
            url=item['url'],
            name=item['name'],
            content=item['content'],
            origin=item['origin'],
            last_updated=item['last_updated'],
            attfl=item['attfl']
        )
    else:
        return None


def get_item_class(instance):

    if instance == 'webservices-search':
        return IndexerWebServicesItem
