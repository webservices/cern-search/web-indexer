#!/usr/bin/python
import logging
import ast
from threading import RLock


class BucketList:
    class __BucketList:

        def __init__(self):
            self.lock = RLock()
            self.bucket = {}

        def put(self, key, value):
            if self.lock.acquire(blocking=1):
                self.bucket[key] = value
                self.lock.release()
            else:
                logging.error('LockError: Unable to put {key} - {val} in the bucket list'.format(key=key, val=value))
                pass

        def remove(self, key):
            if self.lock.acquire(blocking=1):
                self.bucket.pop(key)
                self.lock.release()
            else:
                logging.error('LockError: Unable to remove {key} from the bucket list'.format(key=key))
                pass

        def exists(self, key):
            if self.lock.acquire(blocking=1):
                ret = self.bucket.get(key) is not None
                self.lock.release()
                return ret
            else:
                logging.error('LockError: Unable to check existence of {key} in the bucket list'.format(key=key))
                pass

    instance = None

    def __init__(self):
        if not BucketList.instance:
            BucketList.instance = BucketList.__BucketList()

    def __getattr__(self, name):
        return getattr(self.instance, name)


def evaluate_config_param(param):
    # Evaluate value
    try:
        return ast.literal_eval(param)
    except (SyntaxError, ValueError):
        return None
