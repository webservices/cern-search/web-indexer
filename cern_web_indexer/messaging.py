#!/usr/bin/python
import socket
import import_string
import stomp

from cern_web_indexer import config


# Brokers are behing a DNS load balancer, therefore it balancing is left to the Messaging Service


def get_broker(hostname, port):
    try:
        return [(socket.gethostbyname(hostname), port)]
    except (ValueError, socket.error, socket.gaierror, socket.herror, socket.timeout):
        # TODO: Log error message
        return None


def get_broker_list(hostname, port):
    try:
        resp = socket.gethostbyname_ex(hostname)
        if len(resp) == 3:
            return [(ip, port) for ip in resp[2]]
        else:
            # TODO: Log error message
            return None
    except (ValueError, socket.error, socket.gaierror, socket.herror, socket.timeout):
        # TODO: Log error message
        return None


# Cannot be set up as singleton since it has to listen different queues with different listeners.
class BrokerConnection:
    def __init__(self, broker_loader_function=None):
        if not broker_loader_function:
            broker_loader_function = config.broker_list_loader_function
        broker_loader_function = import_string(broker_loader_function)
        self.conn = stomp.Connection12(
            host_and_ports=broker_loader_function(config.broker_hostname, config.broker_port),
            use_ssl=True,
            ssl_cert_file=config.broker_ssl_cert)

    def set_listener(self, name, listener):
        self.conn.set_listener(name, listener)

    def start(self):
        self.conn.start()

    def connect(self):
        if not self.conn.is_connected():
            self.conn.connect(wait=True)

    def disconnect(self):
        self.conn.disconnect()

    def subscribe(self, queue, id, ack):
        self.conn.subscribe(destination=queue, id=id, ack=ack)

    def unsubscribe(self, id):
        self.conn.unsubscribe(id)

    def send(self, message, queue):
        self.conn.send(body=message, destination=queue)
