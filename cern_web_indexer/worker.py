#!/usr/bin/python
import time

from cern_web_indexer import config
from cern_web_indexer.listeners import TodoListener, DoneListener
from cern_web_indexer.messaging import BrokerConnection

# Create connection
todo_broker = BrokerConnection()

# Set todo listener
todo_broker.set_listener('todo', TodoListener())
todo_broker.start()
todo_broker.connect()
todo_broker.subscribe(config.todo_queue, 'todo', 'auto')

# Do not exit program
# Disconnect upon signal
try:
    print("Connected & Listening sir!")
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    print("Disconnecting receiver...")
    time.sleep(1)
    todo_broker.unsubscribe('todo')
    todo_broker.disconnect()
