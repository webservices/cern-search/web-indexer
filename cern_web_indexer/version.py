#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Version information for CERN Web Indexer.
This file is imported by ``web_indexer.__init__``,
and parsed by ``setup.py``.
"""

from __future__ import absolute_import, print_function

__version__ = '0.0.1a'