#!/usr/bin/python
import hashlib
import logging
import json
import urllib

import import_string
import requests

from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.orm import sessionmaker
from stomp import ConnectionListener

from cern_web_indexer import config
from cern_web_indexer.messaging import BrokerConnection
from cern_web_indexer.models import db_connect, create_indexer_instance_item_table, create_item, \
    get_item_class
from cern_web_indexer.utils import BucketList


# Listeners


class FeedListener(ConnectionListener):

    def __init__(self):
        ConnectionListener.__init__(self)
        self.broker_connection = BrokerConnection(
            broker_loader_function=config.broker_loader_function
        )
        self.bucketlist = BucketList()
        engine = db_connect()
        create_indexer_instance_item_table(engine)
        self.Session = sessionmaker(bind=engine)

    def on_error(self, headers, message):
        logging.error('FeedListener: Received an error {msg}'.format(msg=message))

    def on_message(self, headers, message):
        # TODO change for logging.debug all print statements
        print('FeedListener: received a message "%s"' % message)
        # Create DB session
        session = self.Session()
        try:
            # Parse message
            json_msg = json.loads(message)
            # Set document in todo list
            key = hashlib.md5('message').hexdigest()
            self.bucketlist.put(key, message)
            json_msg['key'] = key
            # Manipulate document
            item = create_item(json_msg)

            if item:
                # TODO Integrity check. Query for existing record with same URL/Origin
                # Update database
                item.key=json_msg['key']
                session.add(item)
                session.commit()  # Commit before sending in case of error
                session.close()
                # Send document to todo queue
                self.broker_connection.start()
                self.broker_connection.connect()
                self.broker_connection.send(message=json.dumps(json_msg), queue=config.todo_queue)
            else:
                logging.error('Instance {instance} is not configured'.format(instance=json_msg['instance']))
                session.close()
        except ValueError:
            logging.error('Unable to parse message {msg}'.format(msg=message))
            session.rollback()
        except InvalidRequestError:
            session.rollback()
        finally:
            session.close()
            self.broker_connection.disconnect()


class TodoListener(ConnectionListener):

    def __init__(self):
        ConnectionListener.__init__(self)
        self.broker_connection = BrokerConnection(
            broker_loader_function=config.broker_loader_function
        )

    def on_error(self, headers, message):
        logging.error('TodoListener: Received an error {msg}'.format(msg=message))

    def on_message(self, headers, message):
        print('TodoListener: received a message "%s"' % message)
        # Parse message
        json_msg = json.loads(message)
        search_instance = config.cernsearch.get(json_msg['instance'])
        if search_instance:
            # Query CERN Search REST API
            headers = {
                "Accept": "application/json",
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": 'Bearer {credentials}'.format(credentials=search_instance['token'])
            }
            url = urllib.quote(json_msg['url'], safe ='')
            origin = urllib.quote(json_msg['origin'].replace('/','\/'), safe='')
            # Check if it's an extra (attachment/followed link)
            if json_msg['attfl']:
                ack = self.process_attachment(headers,
                                              url,
                                              self.get_doc(headers, origin, search_instance),
                                              json_msg['content'],
                                              search_instance)
            else:
                ack = self.process_origin(headers,
                                          self.get_doc(headers, origin, search_instance),
                                          json_msg,
                                          search_instance)
            if ack:
                key_msg = {"key": json_msg['key'], "instance": json_msg['instance']}
                self.broker_connection.start()
                self.broker_connection.connect()
                self.broker_connection.send(message=json.dumps(key_msg), queue=config.done_queue)
        else:
            logging.error('Instance {instance} not configured'.format(instance=json_msg['instance']))

    def get_doc(self, headers, url, search_instance):
        resp = requests.get('{api}/api/records/?q=url:{url}+AND+origin:{url}'.format(
            api=search_instance['api'],
            url=url
        ), headers=headers)

        if resp.status_code != 200:
            # This means something went wrong.
            raise requests.RequestException(
                'GET {api}/api/records/?q=url:{url}+AND+origin:{url} {resp_code}'.format(
                    api=search_instance['api'],
                    url=url,
                    resp_code=resp.status_code
                )
            )

        return json.loads(resp.content)['hits']

    def process_attachment(self, headers, url, hits, content, search_instance):
        # Check if the parent exists or not
        num_docs = hits['total']
        if num_docs > 0:  # Else means no hits, therefore origin doc doesn't exist
            doc = hits['hits'][0]['metadata']
            if num_docs > 1:
                raise requests.RequestException(
                    'More than one hit when it should be unique. HTTP GET {api}/?q=url:{origin}'
                    '+AND+origin:{origin}'.format(
                        api=search_instance['api'],
                        origin=doc['origin']
                    )
                )
            else:
                # Parent exists modify if needed, append if not existent.
                # extras always exists, but might be length 0
                extra_content = doc['extras'].get(url)
                if extra_content:
                    if extra_content != content:
                        body = [{"op": "replace",
                                 "path": "/extras/{extra}".format(extra=url),
                                 "value": content
                                 }]
                        return self.patch_doc(search_instance, doc['control_number'], body, headers)
                    logging.warn('Nothing to update but document reached indexer. {rid} -\n {content}'.format(
                        rid=doc['control_number'],
                        content=content
                    ))
                    return True
                else:
                    # Add the extra, doesn't exist
                    body = [{"op": "add",
                             "path": "/extras/{extra}".format(extra=url),
                             "value": content
                             }]
                    return self.patch_doc(search_instance, doc['control_number'], body, headers)
        return False

    def process_origin(self, headers, api_doc, new_doc, search_instance):
        # Check if the parent exists or not
        num_docs = api_doc['total']
        if num_docs > 0:  # Else means no hits, therefore doc doesn't exist
            api_doc = api_doc['hits'][0]['metadata']
            if num_docs > 1:
                raise requests.RequestException(
                    'More than one hit when it should be unique. HTTP GET {api}/?q=url:{origin}'
                    '+AND+origin:{origin}'.format(
                        api=search_instance['api'],
                        origin=api_doc['origin']
                    )
                )
            else:  # Document exists, therefore update it
                if new_doc['content'] != api_doc['content'] or new_doc['name'] != api_doc['name']:
                    body = [
                        {
                            "op": "replace",
                            "path": "/content",
                            "value": new_doc['content']
                        },
                        {
                            "op": "replace",
                            "path": "/last_updated",
                            "value": new_doc['last_updated']
                        },
                        {
                            "op": "replace",
                            "path": "/name",
                            "value": new_doc['name']
                        }
                    ]
                    return self.patch_doc(search_instance, api_doc['control_number'], body, headers)
                else:
                    logging.warn('Nothing to update but document reached indexer. {rid} -\n {content}'.format(
                        rid=api_doc['control_number'],
                        content=str(new_doc)
                    ))
                    return True
        # Document doesn't exist
        else:
            body = {
                "_access": {
                    "owner": ["CernSearch-Administrators@cern.ch"],
                    "update": ["CernSearch-Administrators@cern.ch"],
                    "delete": ["CernSearch-Administrators@cern.ch"]
                },
                "name": new_doc['name'],
                "url": new_doc['url'],
                "origin": new_doc['origin'],
                "last_updated": new_doc['last_updated'],
                "content": new_doc['content'],
                "extras": {},
            }

            resp = requests.post('{api}/api/records/'.format(
                    api=search_instance['api']
            ), headers=headers, data=json.dumps(body))
            if resp.status_code != 201:
                raise requests.RequestException(
                    'Unable to POST document \n{body}'.format(
                        body=body
                    )
                )
            return True

    def patch_doc(self, search_instance, rid, body, headers):
        # Updated, replace extra content
        headers['Content-Type'] = 'application/json-patch+json'
        resp = requests.patch('{api}/api/record/{rid}'.format(
                api=search_instance['api'],
                rid=rid
        ), headers=headers, data=json.dumps(body))

        if resp.status_code != 200:
            logging.warn('Unable to PATCH document {rid} with jsonpatch \n{body}'.format(
                    body=body,
                    rid=rid)
            )
            return False
        return True


class DoneListener(ConnectionListener):

    def __init__(self):
        ConnectionListener.__init__(self)
        self.bucketlist = BucketList()
        engine = db_connect()
        create_indexer_instance_item_table(engine)
        self.Session = sessionmaker(bind=engine)

    def on_error(self, headers, message):
        logging.error('DoneListener: Received an error {msg}'.format(msg=message))

    def on_message(self, headers, message):
        print('DoneListener: received a message "%s"' % message)
        session = self.Session()
        # Parse message
        try:
            # Get document ID
            json_msg = json.loads(message)
            item_class = get_item_class(json_msg['instance'])
            # Update database
            item = session.query(item_class).filter(item_class.key == json_msg['key']).first()
            if item:
                session.delete(item)
                session.commit()  # Commit before sending in case of error
                # Remove from bucketlist
                self.bucketlist.remove(json_msg['key'])
            else:
                logging.error('SQL Error: Received a key that does not exists in DB {msg}'.format(msg=message))
        except ValueError:
            logging.error('Unable to parse message {msg}'.format(msg=message))
