# -*- coding: utf-8 -*-

# Use CentOS7:
FROM cern/cc7-base

# Install pre-requisites
RUN yum update -y && \
    yum install -y \
    	gcc \
    	python-devel \
        python-pip \
        wget \
        make \
        python-virtualenv && \
    wget https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tgz && \
    tar xf Python-2.7.15.tgz && \
    cd Python-2.7.15 && \
    ./configure --prefix=/python2.7.15 --enable-unicode=ucs4 --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib" && \
    make && make altinstall && \
    cd .. && \
    virtualenv --python=/python2.7.15/bin/python2.7 webindexer && \
    source ./webindexer/bin/activate && \
    pip install --upgrade pip

ADD requirements.txt /tmp
RUN source ./webindexer/bin/activate && pip install -r /tmp/requirements.txt

ADD . /indexer
WORKDIR /indexer/

RUN source /webindexer/bin/activate && pip install -e .

WORKDIR /indexer/cern_web_indexer

CMD ["source /webindexer/bin/activate && python manager.py"]